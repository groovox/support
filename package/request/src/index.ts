import axios, { AxiosRequestConfig } from "axios";
import cheerio from "cheerio";

export type CheerioOption = Parameters<typeof cheerio.load>[1];
export type CheerioRoot = ReturnType<typeof cheerio.load>;

export const getHtml = async (
  url: string,
  config?: AxiosRequestConfig,
  option?: CheerioOption
): Promise<CheerioRoot> => {
  const res = await axios.get(url, config);
  return cheerio.load(res.data, option);
};

export const getJson = async (
  url: string,
  config?: AxiosRequestConfig
): Promise<any> => {
  const res = await axios.get(url, config);
  return res.data;
};
