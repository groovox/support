# @groovox/request

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines] [![NPM][npm_badge]][npm]

Request for Groovox.

## Getting Started

```
npm i @groovox/request
```

[license]: https://gitlab.com/groovox/support/blob/master/LICENSE
[license_badge]: https://img.shields.io/npm/l/@groovox/request
[pipelines]: https://gitlab.com/groovox/support/pipelines
[pipelines_badge]: https://gitlab.com/groovox/support/badges/master/pipeline.svg
[npm]: https://www.npmjs.com/package/@groovox/request
[npm_badge]: https://img.shields.io/npm/v/@groovox/request/latest.svg
