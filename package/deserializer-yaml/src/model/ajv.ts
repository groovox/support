import Ajv from "ajv";
import addFormats from "ajv-formats";

import { schema } from "./schema";

export enum Schema {
  show = "show",
  movie = "movie",
  album = "album",
  artist = "artist",
  episode = "episode",
  actor = "actor"
}

export const createAjv = (): Ajv => {
  const ajv = new Ajv();
  addFormats(ajv);
  ajv.addSchema(schema.definitions);
  ajv.addSchema(schema.actor, Schema.actor);
  ajv.addSchema(schema.show, Schema.show);
  ajv.addSchema(schema.movie, Schema.movie);
  ajv.addSchema(schema.album, Schema.album);
  ajv.addSchema(schema.artist, Schema.artist);
  ajv.addSchema(schema.episode, Schema.episode);
  return ajv;
};
