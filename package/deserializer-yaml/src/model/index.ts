import { createAjv, Schema } from "./ajv";
import { Actor, Album, Episode, Movie, Show } from "./model";

const ajv = createAjv();

const validateModel = <T>(schema: Schema) => (value: unknown): value is T => {
  const validate = ajv.getSchema<Actor>(schema);
  return Boolean(validate?.(value));
};

export const isActor = validateModel<Actor>(Schema.actor);
export const isShow = validateModel<Show>(Schema.show);
export const isMovie = validateModel<Movie>(Schema.movie);
export const isAlbum = validateModel<Album>(Schema.album);
export const isArtist = validateModel<Actor>(Schema.artist);
export const isEpisode = validateModel<Episode>(Schema.episode);

export * from "./model";
