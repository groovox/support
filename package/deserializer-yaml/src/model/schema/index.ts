import definitions from "./defs.schema.json";
import actor from "./actor.schema.json";
import show from "./show.schema.json";
import movie from "./movie.schema.json";
import album from "./album.schema.json";
import artist from "./artist.schema.json";
import episode from "./episode.schema.json";

export const schema = {
  definitions,
  actor,
  show,
  movie,
  album,
  artist,
  episode
};
