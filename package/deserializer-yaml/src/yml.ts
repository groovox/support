import * as yaml from "yaml";
import {
  isActor,
  isAlbum,
  isArtist,
  isEpisode,
  isMovie,
  isShow
} from "./model";

const getYml = <T>(isFn: (value: unknown) => value is T) => (
  yml: string
): T | undefined => {
  const value = yaml.parse(yml);
  return isFn(value) ? value : undefined;
};

export const getActor = getYml(isActor);
export const getAlbum = getYml(isAlbum);
export const getArtist = getYml(isArtist);
export const getEpisode = getYml(isEpisode);
export const getMovie = getYml(isMovie);
export const getShow = getYml(isShow);
