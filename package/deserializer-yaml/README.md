# @groovox/deserializer-yaml

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines] [![NPM][npm_badge]][npm]

Legacy YAML deserializer for Groovox.

## Getting Started

```
npm i @groovox/deserializer-yaml
```

[license]: https://gitlab.com/groovox/support/blob/master/LICENSE
[license_badge]: https://img.shields.io/npm/l/@groovox/deserializer-yaml
[pipelines]: https://gitlab.com/groovox/support/pipelines
[pipelines_badge]: https://gitlab.com/groovox/support/badges/master/pipeline.svg
[npm]: https://www.npmjs.com/package/@groovox/deserializer-yaml
[npm_badge]: https://img.shields.io/npm/v/@groovox/deserializer-yaml/latest.svg
