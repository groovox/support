# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.2.2](https://gitlab.com/groovox/support/compare/v1.2.1...v1.2.2) (2021-01-25)

**Note:** Version bump only for package @groovox/deserializer-yaml





## [1.1.6](https://gitlab.com/groovox/support/compare/v1.1.5...v1.1.6) (2021-01-18)

**Note:** Version bump only for package @groovox/deserializer-yaml





## [1.1.5](https://gitlab.com/groovox/support/compare/v1.1.4...v1.1.5) (2021-01-13)

**Note:** Version bump only for package @groovox/deserializer-yaml

## [1.1.1](https://gitlab.com/groovox/support/compare/v1.1.0...v1.1.1) (2021-01-13)

**Note:** Version bump only for package @groovox/deserializer-yaml

# 1.1.0 (2021-01-13)

### Features

- update request ([41c92e8](https://gitlab.com/groovox/support/commit/41c92e8cbf663ebcf2fe49054e3002eaffa143ec))
