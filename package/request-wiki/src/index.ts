import { parseTable } from "@joshuaavalon/cheerio-table-parser";
import { Element } from "domhandler";
import { CheerioRoot } from "@groovox/request";
import _ from "lodash";
import { DateTime } from "luxon";
import $ from "cheerio";

export interface WikiData {
  title: string[];
  aired: string;
  directors: string[];
  writers: string[];
}

export interface Option {
  id: string;
  offset?: number;
  multipleRow?: number;
  mapping?: Partial<{
    title: number;
    aired: number;
    directors: number;
    writers: number;
  }>;
  parsers?: Partial<{
    title: (elements: Element[]) => string[];
    aired: (elements: Element[]) => string;
    directors: (elements: Element[]) => string[];
    writers: (elements: Element[]) => string[];
  }>;
  validate?: (table: Element[][]) => void;
}

const mapMultipleRows = (
  table: Element[][],
  multipleRow?: number
): number[][] => {
  if (_.isUndefined(multipleRow)) {
    return _.range(table.length).map(i => [i]);
  }
  const rowMap: number[][] = [];
  let index = 0;
  let prev: Element | undefined = undefined;
  table.forEach((cols, row) => {
    if (!_.isArray(rowMap[index])) {
      rowMap[index] = [];
    }
    const current = cols[multipleRow];
    if (_.eq(current, prev) || row === 0) {
      rowMap[index].push(row);
    } else {
      index++;
      rowMap[index] = [row];
    }
    prev = current;
  });
  return rowMap;
};

const normalizeElement = (element: Element): void => {
  $("sup", element).remove();
  $("style", element).remove();
  $("ruby", element).each((_, ruby): void => {
    if ($("rp", ruby).length > 0) {
      return;
    }
    $("rt", ruby).prepend("(").append(")");
  });
  $("br", element).replaceWith("\n");
  $("hr", element).replaceWith("\n");
  $("li", element).append("\n");
};

const parseSimpleString = (elements: Element[]): string[] =>
  _.flatMap(elements, e => {
    normalizeElement(e);
    return $(e).text().replace(/\n+/gu, " ");
  });

const parseSimpleDate = (values: Element[]): string =>
  DateTime.fromISO($(values[0]).text()).toISODate();

const parseSpaceString = (elements: Element[]): string[] => {
  const result = _.flatMap(elements, e =>
    $(e)
      .text()
      .split(/\s+/u)
      .filter(v => v)
  );
  return _.uniq(result);
};

const createData = (
  data: WikiData,
  option: Option,
  table: Element[][],
  rowMap: number[]
): WikiData => {
  const { mapping = {}, parsers = {} } = option;
  const {
    title: parseTitle = parseSimpleString,
    aired: parseAired = parseSimpleDate,
    directors: parseDirectors = parseSpaceString,
    writers: parseWriters = parseSpaceString
  } = parsers;
  const { title, aired, directors, writers } = mapping;
  const values: Element[][] = [];
  rowMap.forEach(row => {
    table[row].forEach((col, i) => {
      if (!_.isArray(values[i])) {
        values[i] = [];
      }
      values[i] = [...values[i], col];
    });
  });
  if (_.isNumber(title)) {
    data.title = parseTitle(values[title]);
  }
  if (_.isNumber(aired)) {
    data.aired = parseAired(values[aired]);
  }
  if (_.isNumber(directors)) {
    data.directors = parseDirectors(values[directors]);
  }
  if (_.isNumber(writers)) {
    data.writers = parseWriters(values[writers]);
  }
  return data;
};

export const transformWikiDataHook = (option: Option) => (
  dom: CheerioRoot,
  episode: number,
  data: WikiData
): WikiData => {
  const { id, offset = 0, multipleRow, validate } = option;
  const tables = dom(`#${id}`).parent().nextAll("table.wikitable").toArray();
  if (tables.length <= offset) {
    throw new Error(`There are only ${tables.length} table(s).`);
  }
  const tableNode = tables[offset];
  const table = parseTable(tableNode, { parser: e => e });
  validate?.(table);
  const rowNum = episode;
  const rowMap = mapMultipleRows(table, multipleRow)[rowNum];
  return createData(data, option, table, rowMap);
};

export const getUrl = (language: string, topic: string): string =>
  `https://${language}.wikipedia.org/wiki/${topic}`;

export const validateText = (cells: [number, number, string][]) => (
  table: Element[][]
): void => {
  cells.forEach(([row, col, except]) => {
    const value = $(table[row][col]).text();
    if (value !== except) {
      throw new Error(`Except "${value}" to be "${except}"`);
    }
  });
};
