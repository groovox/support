# @groovox/request-wiki

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines] [![NPM][npm_badge]][npm]

Wiki request for Groovox.

## Getting Started

```
npm i @groovox/request-wiki
```

[license]: https://gitlab.com/groovox/support/blob/master/LICENSE
[license_badge]: https://img.shields.io/npm/l/@groovox/request-wiki
[pipelines]: https://gitlab.com/groovox/support/pipelines
[pipelines_badge]: https://gitlab.com/groovox/support/badges/master/pipeline.svg
[npm]: https://www.npmjs.com/package/@groovox/request-wiki
[npm_badge]: https://img.shields.io/npm/v/@groovox/request-wiki/latest.svg
