# @groovox/tmdb

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines] [![NPM][npm_badge]][npm]

The Movie Database (TMDb) Api wrapper.

## Getting Started

```
npm i @groovox/tmdb
```

```typescript
import { TmdbApiV3 } from "@groovox/tmdb";

const client = new TmdbApiV3({ auth: { accessToken: "<ACCESS_TOKEN>" } });
// const client = new TmdbApiV3({ auth: { apiKey: "<API_KEY>" } });

const result = await client.searchCompanies({ query: "Example" });
```

## Implemented Api

- [searchCompanies](https://developers.themoviedb.org/3/search/search-companies)
- [searchCollections](https://developers.themoviedb.org/3/search/search-collections)
- [searchKeywords](https://developers.themoviedb.org/3/search/search-keywords)
- [searchMovies](https://developers.themoviedb.org/3/search/search-movies)
- [multiSearch](https://developers.themoviedb.org/3/search/multi-search)
- [searchPeople](https://developers.themoviedb.org/3/search/search-people)
- [searchTvShows](https://developers.themoviedb.org/3/search/search-tv-shows)
- [getTvDetails](https://developers.themoviedb.org/3/tv/get-tv-details)
- [getTvSeasonDetails](https://developers.themoviedb.org/3/tv-seasons/get-tv-season-details)
- [getMovieDetails](https://developers.themoviedb.org/3/movies/get-movie-details)

[license]: https://gitlab.com/groovox/support/blob/master/LICENSE
[license_badge]: https://img.shields.io/npm/l/@groovox/tmdb
[pipelines]: https://gitlab.com/groovox/support/pipelines
[pipelines_badge]: https://gitlab.com/groovox/support/badges/master/pipeline.svg
[npm]: https://www.npmjs.com/package/@groovox/tmdb
[npm_badge]: https://img.shields.io/npm/v/@groovox/tmdb/latest.svg
