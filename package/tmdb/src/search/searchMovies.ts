import { AxiosInstance, AxiosResponse } from "axios";
import { ApiResponse, MultilingualRequest } from "../base";
import {
  AdultSearchRequest,
  RegionSearchRequest,
  SearchRequest
} from "./model";

export interface RequestParameters
  extends AdultSearchRequest,
    MultilingualRequest,
    RegionSearchRequest,
    SearchRequest {
  year?: number;
  primary_release_year?: number;
}

export type ResponseResult = ApiResponse<Movie>;

export interface Movie {
  adult: boolean;
  backdrop_path?: string | null;
  genre_ids: number[];
  id: number;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path?: string | null;
  release_date: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
}

export const searchMovies = (axios: AxiosInstance) => async (
  params: RequestParameters
): Promise<AxiosResponse<ResponseResult>> =>
  axios.get<ResponseResult>("/search/movie", { params });
