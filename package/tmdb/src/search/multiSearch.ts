import { AxiosInstance, AxiosResponse } from "axios";
import { ApiResponse, MultilingualRequest } from "../base";
import {
  AdultSearchRequest,
  RegionSearchRequest,
  SearchRequest
} from "./model";
import { Movie } from "./searchMovies";
import { Person } from "./searchPeople";
import { TvShow } from "./searchTvShows";

export interface RequestParameters
  extends AdultSearchRequest,
    MultilingualRequest,
    RegionSearchRequest,
    SearchRequest {}

export type ResponseResult = ApiResponse<Movie | Person | TvShow>;

export const multiSearch = (axios: AxiosInstance) => async (
  params: RequestParameters
): Promise<AxiosResponse<ResponseResult>> =>
  axios.get<ResponseResult>("/search/multi", { params });
