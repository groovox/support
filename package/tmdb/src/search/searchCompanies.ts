import { AxiosInstance, AxiosResponse } from "axios";
import { ApiResponse } from "../base";
import { SearchRequest } from "./model";

export interface RequestParameters extends SearchRequest {}

export type ResponseResult = ApiResponse<Company>;

export interface Company {
  id: number;
  logo_path?: string | null;
  name: string;
  origin_country: string;
}

export const searchCompanies = (axios: AxiosInstance) => async (
  params: RequestParameters
): Promise<AxiosResponse<ResponseResult>> =>
  axios.get<ResponseResult>("/search/company", { params });
