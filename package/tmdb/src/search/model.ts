export interface SearchRequest {
  /**
   * Pass a text query to search. This value should be URI encoded.
   */
  query: string;
  /**
   * Specify which page to query.
   */
  page?: number;
}

export interface RegionSearchRequest {
  /**
   * Specify a ISO 3166-1 code to filter release dates. Must be uppercase.
   */
  region?: string;
}

export interface AdultSearchRequest {
  /**
   * Choose whether to inlcude adult (pornography) content in the results.
   */
  include_adult?: boolean;
}
