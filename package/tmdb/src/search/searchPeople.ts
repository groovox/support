import { AxiosInstance, AxiosResponse } from "axios";
import { ApiResponse, MultilingualRequest } from "../base";
import {
  AdultSearchRequest,
  RegionSearchRequest,
  SearchRequest
} from "./model";
import { Movie } from "./searchMovies";
import { TvShow } from "./searchTvShows";

export interface RequestParameters
  extends AdultSearchRequest,
    MultilingualRequest,
    RegionSearchRequest,
    SearchRequest {}

export type ResponseResult = ApiResponse<Person>;

export interface Person {
  profile_path?: string | null;
  adult: boolean;
  id: number;
  name: string;
  popularity: number;
  known_for: (Movie | TvShow)[];
  known_for_department: string;
}

export const searchPeople = (axios: AxiosInstance) => async (
  params: RequestParameters
): Promise<AxiosResponse<ResponseResult>> =>
  axios.get<ResponseResult>("/search/person", { params });
