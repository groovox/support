import { AxiosInstance, AxiosResponse } from "axios";
import { ApiResponse, MultilingualRequest } from "../base";
import { SearchRequest } from "./model";

export interface RequestParameters extends MultilingualRequest, SearchRequest {}

export type ResponseResult = ApiResponse<Collection>;

export interface Collection {
  id: number;
  name: string;
  backdrop_path?: string | null;
  poster_path?: string | null;
}

export const searchCollections = (axios: AxiosInstance) => async (
  params: RequestParameters
): Promise<AxiosResponse<ResponseResult>> =>
  axios.get<ResponseResult>("/search/collection", { params });
