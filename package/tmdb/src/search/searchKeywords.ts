import { AxiosInstance, AxiosResponse } from "axios";
import { ApiResponse } from "../base";
import { SearchRequest } from "./model";

export interface RequestParameters extends SearchRequest {}

export type ResponseResult = ApiResponse<Keyword>;

export interface Keyword {
  id: number;
  name: string;
}

export const searchKeywords = (axios: AxiosInstance) => async (
  params: RequestParameters
): Promise<AxiosResponse<ResponseResult>> =>
  axios.get<ResponseResult>("/search/keyword", { params });
