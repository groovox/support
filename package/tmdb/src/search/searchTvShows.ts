import { AxiosInstance, AxiosResponse } from "axios";
import { ApiResponse, MultilingualRequest } from "../base";
import {
  AdultSearchRequest,
  RegionSearchRequest,
  SearchRequest
} from "./model";

export interface RequestParameters
  extends AdultSearchRequest,
    MultilingualRequest,
    RegionSearchRequest,
    SearchRequest {
  first_air_date_year?: number;
}

export type ResponseResult = ApiResponse<TvShow>;

export interface TvShow {
  poster_path?: string | null;
  popularity: number;
  id: number;
  backdrop_path?: string | null;
  vote_average: number;
  overview: string;
  first_air_date: string;
  origin_country: string[];
  genre_ids: number[];
  original_language: string;
  vote_count: number;
  name: string;
  original_name: string;
}

export const searchTvShows = (axios: AxiosInstance) => async (
  params: RequestParameters
): Promise<AxiosResponse<ResponseResult>> =>
  axios.get<ResponseResult>("/search/show", { params });
