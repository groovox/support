export interface ErrorResponse {
  status_code: number;
  status_message: string;
}

export interface ApiResponse<T> {
  page: number;
  results: T[];
  total_pages: number;
  total_results: number;
}

export interface ApiRequest {
  append_to_response?: string[];
  include_image_language?: string[];
}

export interface MultilingualRequest {
  /**
   * Pass a ISO 639-1 value to display translated data for the fields that support it.
   */
  language?: string;
}
