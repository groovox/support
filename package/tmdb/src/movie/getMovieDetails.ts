import { AxiosInstance, AxiosResponse } from "axios";
import { ApiRequest, ApiResponse, MultilingualRequest } from "../base";
import { Collection } from "../search";

export interface RequestParameters extends ApiRequest, MultilingualRequest {}

export type ResponseResult = ApiResponse<MovieDetails>;

export interface Genre {
  id: number;
  name: string;
}

export interface ProductionCompany {
  id: number;
  logo_path?: string | null;
  name: string;
  origin_country: string;
}

export interface ProductionCountry {
  iso_3166_1: string;
  name: string;
}

export interface SpokenLanguage {
  iso_639_1: string;
  name: string;
}

export interface MovieDetails {
  adult: boolean;
  backdrop_path?: string | null;
  belongs_to_collection?: Collection;
  budget: number;
  genres: Genre[];
  homepage: string;
  id: number;
  imdb_id: string;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path?: string | null;
  production_companies: ProductionCompany[];
  production_countries: ProductionCountry[];
  release_date: string;
  revenue: number;
  runtime: number;
  spoken_languages: SpokenLanguage[];
  status: string;
  tagline: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
}

export const getMovieDetails = (axios: AxiosInstance) => async (
  movieId: string,
  params: RequestParameters
): Promise<AxiosResponse<ResponseResult>> =>
  axios.get<ResponseResult>(`/movie/${movieId}`, { params });
