import axios, { AxiosInstance, AxiosRequestConfig } from "axios";

import {
  multiSearch,
  searchCollections,
  searchCompanies,
  searchKeywords,
  searchMovies,
  searchPeople,
  searchTvShows
} from "./search";

import { getTvDetails, getTvSeasonDetails } from "./tv";
import { getMovieDetails } from "./movie";

export type ApiKeyAuth = {
  apiKey: string;
};

export type AccessTokenAuth = {
  accessToken: string;
};

export interface TmdbApiV3Option {
  auth: ApiKeyAuth | AccessTokenAuth;
  axiosConfig?: AxiosRequestConfig;
}

export class TmdbApiV3 {
  private axios: AxiosInstance;
  /**
   * Search for companies.
   * @link https://developers.themoviedb.org/3/search/search-companies
   */
  public searchCompanies: ReturnType<typeof searchCompanies>;
  /**
   * Search for collections.
   * @link https://developers.themoviedb.org/3/search/search-collections
   */
  public searchCollections: ReturnType<typeof searchCollections>;
  /**
   * Search for keywords.
   * @link https://developers.themoviedb.org/3/search/search-keywords
   */
  public searchKeywords: ReturnType<typeof searchKeywords>;
  /**
   * Search for movies.
   * @link https://developers.themoviedb.org/3/search/search-movies
   */
  public searchMovies: ReturnType<typeof searchMovies>;
  /**
   * Search multiple models in a single request.
   * Multi search currently supports searching for movies, tv shows and people in a single request.
   * @link https://developers.themoviedb.org/3/search/multi-search
   */
  public multiSearch: ReturnType<typeof multiSearch>;
  /**
   * Search for people.
   * @link https://developers.themoviedb.org/3/search/search-people
   */
  public searchPeople: ReturnType<typeof searchPeople>;
  /**
   * Search for a TV show.
   * @link https://developers.themoviedb.org/3/search/search-tv-shows
   */
  public searchTvShows: ReturnType<typeof searchTvShows>;
  /**
   * Get the primary TV show details by id.
   * @link https://developers.themoviedb.org/3/tv/get-tv-details
   */
  public getTvDetails: ReturnType<typeof getTvDetails>;
  /**
   * Get the TV season details by id.
   * @link https://developers.themoviedb.org/3/tv-seasons/get-tv-season-details
   */
  public getTvSeasonDetails: ReturnType<typeof getTvSeasonDetails>;
  /**
   * Get the TV season details by id.
   * @link https://developers.themoviedb.org/3/movies/get-movie-details
   */
  public getMovieDetails: ReturnType<typeof getMovieDetails>;

  constructor(opt: TmdbApiV3Option) {
    const { auth, axiosConfig } = opt;
    this.axios = axios.create({
      baseURL: "https://api.themoviedb.org/3/",
      ...axiosConfig
    });
    this.axios.defaults.headers["Accept"] = "application/json";
    if ("apiKey" in auth) {
      this.axios.defaults.params = {};
      this.axios.defaults.params["api_key"] = auth.apiKey;
    } else {
      const { accessToken } = auth;
      this.axios.defaults.headers["Authorization"] = `Bearer ${accessToken}`;
    }
    this.searchCompanies = searchCompanies(this.axios);
    this.searchCollections = searchCollections(this.axios);
    this.searchKeywords = searchKeywords(this.axios);
    this.searchMovies = searchMovies(this.axios);
    this.multiSearch = multiSearch(this.axios);
    this.searchPeople = searchPeople(this.axios);
    this.searchTvShows = searchTvShows(this.axios);
    this.getTvDetails = getTvDetails(this.axios);
    this.getTvSeasonDetails = getTvSeasonDetails(this.axios);
    this.getMovieDetails = getMovieDetails(this.axios);
  }
}

export { ErrorResponse } from "./base";
