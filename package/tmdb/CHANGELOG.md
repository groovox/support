# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.2.0](https://gitlab.com/groovox/support/compare/v1.1.6...v1.2.0) (2021-01-18)


### Features

* **tmdb:** add getMovieDetails ([c469f8d](https://gitlab.com/groovox/support/commit/c469f8dccca9a31d20e0f36a8f4e5c05573d9067))





## [1.1.6](https://gitlab.com/groovox/support/compare/v1.1.5...v1.1.6) (2021-01-18)

**Note:** Version bump only for package @groovox/tmdb





## [1.1.5](https://gitlab.com/groovox/support/compare/v1.1.4...v1.1.5) (2021-01-13)

**Note:** Version bump only for package @groovox/tmdb

## [1.1.4](https://gitlab.com/groovox/support/compare/v1.1.3...v1.1.4) (2021-01-13)

**Note:** Version bump only for package @groovox/tmdb

## [1.1.3](https://gitlab.com/groovox/support/compare/v1.1.2...v1.1.3) (2021-01-13)

**Note:** Version bump only for package @groovox/tmdb

## [1.1.2](https://gitlab.com/groovox/support/compare/v1.1.1...v1.1.2) (2021-01-13)

### Bug Fixes

- update package.json ([85b5e2c](https://gitlab.com/groovox/support/commit/85b5e2ca03f6f3c959b7d6de653f652e913ab16f))

# 1.1.0 (2021-01-13)

### Features

- tmdb ([701e2f5](https://gitlab.com/groovox/support/commit/701e2f50352835c04e6795d4ec3320907532b2a2))
