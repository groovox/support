# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.2.2](https://gitlab.com/groovox/support/compare/v1.2.1...v1.2.2) (2021-01-25)


### Features

* **graphql:** remove graphql ([5ef425e](https://gitlab.com/groovox/support/commit/5ef425e636d9c1da2182aeb407975c07df5b07c0))





## [1.2.1](https://gitlab.com/groovox/support/compare/v1.2.0...v1.2.1) (2021-01-18)

**Note:** Version bump only for package @groovox/support





# [1.2.0](https://gitlab.com/groovox/support/compare/v1.1.6...v1.2.0) (2021-01-18)


### Features

* **tmdb:** add getMovieDetails ([c469f8d](https://gitlab.com/groovox/support/commit/c469f8dccca9a31d20e0f36a8f4e5c05573d9067))





## [1.1.6](https://gitlab.com/groovox/support/compare/v1.1.5...v1.1.6) (2021-01-18)

**Note:** Version bump only for package @groovox/support





## [1.1.5](https://gitlab.com/groovox/support/compare/v1.1.4...v1.1.5) (2021-01-13)

### Bug Fixes

- dependencies ([3f8268b](https://gitlab.com/groovox/support/commit/3f8268b5ae0e7a80462c80bad1c9337fc67d6cab))

## [1.1.4](https://gitlab.com/groovox/support/compare/v1.1.3...v1.1.4) (2021-01-13)

**Note:** Version bump only for package groovox

## [1.1.3](https://gitlab.com/groovox/support/compare/v1.1.2...v1.1.3) (2021-01-13)

**Note:** Version bump only for package groovox

## [1.1.2](https://gitlab.com/groovox/support/compare/v1.1.1...v1.1.2) (2021-01-13)

### Bug Fixes

- update package.json ([85b5e2c](https://gitlab.com/groovox/support/commit/85b5e2ca03f6f3c959b7d6de653f652e913ab16f))

## [1.1.1](https://gitlab.com/groovox/support/compare/v1.1.0...v1.1.1) (2021-01-13)

**Note:** Version bump only for package groovox

# 1.1.0 (2021-01-13)

### Features

- graphql ([84a03ce](https://gitlab.com/groovox/support/commit/84a03cefe3479f40c3bfa6f4dc6e633c4ebcdf53))
- initial commit ([63de373](https://gitlab.com/groovox/support/commit/63de373f0813ed0fea4c0741779c1a23d1abd030))
- scraper ([3880c48](https://gitlab.com/groovox/support/commit/3880c4892290eb049760b0eeb93ad192c26ebec0))
- tmdb ([701e2f5](https://gitlab.com/groovox/support/commit/701e2f50352835c04e6795d4ec3320907532b2a2))
- update ([17ed518](https://gitlab.com/groovox/support/commit/17ed518a5a05fdc542cdeb67b2415dda38664a14))
- update request ([41c92e8](https://gitlab.com/groovox/support/commit/41c92e8cbf663ebcf2fe49054e3002eaffa143ec))
