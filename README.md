# @groovox/support

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines]

[license]: https://gitlab.com/groovox/support/blob/master/LICENSE
[license_badge]: https://img.shields.io/npm/l/@groovox/tmdb
[pipelines]: https://gitlab.com/groovox/support/pipelines
[pipelines_badge]: https://gitlab.com/groovox/support/badges/master/pipeline.svg
